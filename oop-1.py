
class Teams:
	'''
	Classe que representa uma seleção da Liga das Nações de Volei.
	Atributos: name : string, points: int, wins : int, defeats : int, matches : int, sets : int.'''

	def __init__(self, name, points, wins, defeats, matches, sets):
		self.name = name			# Nome do país
		self.points = points		# Pontos no campeonato
		self.wins = wins			# Número de vitórias
		self.defeats = defeats		# Número de derrotas
		self.matches = matches		# Número de partidas disputadas
		self.sets = sets 			# Número de sets vencidos

	# Representação oficial do objeto
	def __repr__(self):	
		return self.name

	# Representação 'informal' do objeto (chamado com print(obj), contém descrição detalhada)
	def __str__(self):
		return f'País: {self.name}\nPontos: {self.points}\nVitórias: {self.wins}\nDerrotas: {self.defeats}\
		\nPartidas jogadas: {self.matches}\nSets vencidos: {self.sets}'

	def __add__(self, other):
		return self.points + other.points

	def __sub__(self, other):
		return self.points - other.points

	def __ge__(self, other):
		return self.points >= other.points

	def __le__(self, other):
		return self.points <= other.points

	def __gt__(self, other):
		return self.points > other.points

	def __lt__(self, other):
		return self.points < other.points

	def __eq__(self, other):
		return self.points == other.points

	def __ne__(self, other):
		return self.points != other.points


# Criação dos objetos
brasil = Teams('Brasil', 9, 3, 0, 9, 3)
franca = Teams('França', 6, 2, 1, 6, 3)
polonia = Teams('Polônia', 3, 1, 2, 3, 3)
russia = Teams('Rússia', 0, 1, 3, 1, 3)

# Descrição da equipe
print(brasil)

# Ordenar a lista
teams = [russia, franca, polonia, brasil]
teams.sort()
print(teams)

# Comparação entre equipes
print(brasil == russia)

# Soma e subtração
print(brasil - franca)
print(brasil + polonia)
