import numpy


class Wave:
	'''
	Classe que representa o sinal de um sensor arbitrário.
	Atributos: array : numpy.ndarray, label : string.'''

	def __init__(self, array, label):
		self.array = array
		self.label = label
		self.average = self.array.mean()
		self.rate = 1

	# Representação oficial do objeto (label)
	def __repr__(self):
		return self.label

	# Representação 'informal' do objeto (chamado com print(obj), contém o trecho do sinal)
	def __str__(self):
		return str(self.array)

	def __add__(self, other):
		return Wave(self.array + other.array, f'{self.label} + {other.label}')

	def __sub__(self, other):
		return Wave(self.array - other.array, f'{self.label} - {other.label}')

	def __mul__(self, other):
		return Wave(numpy.append(self.array, other.array), f'{self.label} * {other.label}')

	def __gt__(self, other):
		return self.average > other.average

	def __lt__(self, other):
		return self.average < other.average

	def __eq__(self, other):
		return self.average == other.average

	def __ne__(self, other):
		return self.average != other.average

	def __getitem__(self, key):
		return self.array[key]


def main():
	'''Testa os métodos da classe Wave'''

	arr1 = numpy.random.normal(-1, 1, size=50)
	arr2 = numpy.random.normal(-1, 1, size=50)

	# Define dois objetos Wave
	w1 = Wave(arr1, 'wave 1')
	w2 = Wave(arr2, 'wave 2')
	waves = [w1, w2]

	# Teste da operação print(obj)
	print(f'\n>>> Label: {w1.label}\n')
	print(w1)

	# Teste da operação de soma
	print(f'\n>>> Label: {(w1 + w2).label}\n')
	print(w1 + w2)

	# Teste da operação de subtração
	print(f'\n>>> Label: {(w1 - w2).label}\n')
	print(w1 - w2)

	# Teste da operação de concatenação
	print(f'\n>>> Label: {(w1 * w2).label}\n')
	print(w1 * w2)
	
	# Teste da operação de ordenação da lista de sinais
	waves.sort()
	print(waves)

	# Teste da operação de slicing
	print(f'\n>>> Time slicing\n')
	print(w1[6:12])

	# Teste da soma com slices
	print(f'\n>>> Operação com slicing\n')
	print(w1[6:12] + w2[8:14])


main()
