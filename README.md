# Nautilus Python OO

### oop-1

Este programa implementa uma classe que representa as seleções de um campeonato de vôlei entre nações. O arquivo também contém a criação de alguns objetos dessa classe e testes dos seus métodos.

### oop-2

Este programa implementa uma classe que representa um sinal emitido por um sensor arbitrário. O arquivo também executa uma função que cria dois sinais aleatórios e testa todos os métodos da classe. A biblioteca numpy é necessária para executar este programa.
